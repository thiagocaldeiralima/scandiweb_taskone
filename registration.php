<?php
/**
 * @author Scandiweb Team
 * @copyright Copyright © Scandiweb (https://scandiweb.com)
 */

use \Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(ComponentRegistrar::MODULE, 'Scandiweb_TaskOne', __DIR__);
