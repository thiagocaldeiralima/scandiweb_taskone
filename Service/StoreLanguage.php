<?php
/**
 * @author Scandiweb Team
 * @copyright Copyright © Scandiweb (https://scandiweb.com)
 */
namespace Scandiweb\TaskOne\Service;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\Data\StoreConfig;
use Magento\Store\Model\Service\StoreConfigManager;
use Magento\Store\Model\StoreManagerInterface;

class StoreLanguage
{
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;
    /**
     * @var StoreConfigManager
     */
    private $storeConfigManager;

    /**
     * StoreLanguage constructor.
     * @param StoreManagerInterface $storeManager
     * @param StoreConfigManager $storeConfigManager
     */
    public function __construct(
        StoreManagerInterface $storeManager,
        StoreConfigManager $storeConfigManager
    ) {
        $this->storeManager = $storeManager;
        $this->storeConfigManager = $storeConfigManager;
    }

    /**
     * @return string
     */
    public function getStoreLanguage(): string
    {
        $lang = "";
        try {
            /** @var StoreConfig $storeConfig */
            $storeConfig = current($this->storeConfigManager->getStoreConfigs(
                [$this->storeManager->getStore()->getCode()]
            ));
            $lang = str_replace('_', '-', $storeConfig->getLocale());
        } catch (NoSuchEntityException $e) {
        }
        return strtolower($lang);
    }
}
