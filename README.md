# Scandiweb TaskOne Module

Adds the link relation attribute to CMS pages according to the store language

### How to install

The instalation is plug and play all you need to do is:

##### Automatic way

- composer config repositories.scandiweb/task-one git git@bitbucket.org:thiagocaldeiralima/scandiweb_taskone.git
- composer require scandiweb/task-one:dev-master

##### Manual way

-  copy the code to the `app/code` folder
-  run: `bin/magento setup:upgrade` to install the module

### The expected result

When you hit a cms page should see the store locale was set as:

English (United States) : `<link rel="alternate" hreflang="en-us" href="http://example.com/cms-page-identifier">`

English (United Kingdom) : `<link rel="alternate" hreflang="en-gb" href="http://example.co.uk/cms-page-identifier">`
