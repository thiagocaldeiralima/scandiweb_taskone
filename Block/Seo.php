<?php
/**
 * @author Scandiweb Team
 * @copyright Copyright © Scandiweb (https://scandiweb.com)
 */
namespace Scandiweb\TaskOne\Block;

use Magento\Cms\Model\Page;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\Template;
use Magento\Store\Model\Store;
use Scandiweb\TaskOne\Service\StoreLanguage;

class Seo extends Template
{
    /**
     * @var Page
     */
    private $page;
    /**
     * @var UrlInterface
     */
    private $urlBuilder;
    /**
     * @var StoreLanguage
     */
    private $storeLanguage;

    /**
     * Seo constructor.
     * @param Template\Context $context
     * @param Page $page
     * @param UrlInterface $urlBuilder
     * @param StoreLanguage $storeLanguage
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        Page $page,
        UrlInterface $urlBuilder,
        StoreLanguage $storeLanguage,
        array $data = []
    ) {
        $this->page = $page;
        $this->urlBuilder = $urlBuilder;
        $this->storeLanguage = $storeLanguage;
        parent::__construct($context, $data);
    }

    /**
     * @return bool
     */
    public function displaySeo(): bool
    {
        $cmsStoreIds = $this->page->getStoreId();
        if (!is_array($cmsStoreIds)) {
            $cmsStoreIds = [$cmsStoreIds];
        }
        return in_array(Store::DEFAULT_STORE_ID, $cmsStoreIds) ||
            sizeof($cmsStoreIds) > 1;
    }

    /**
     * @return string
     */
    public function getStoreLanguage(): string
    {
        return $this->storeLanguage->getStoreLanguage();
    }

    /**
     * @return string
     */
    public function getBaseUrl(): string
    {
        return $this->urlBuilder->getBaseUrl();
    }

    /**
     * @return string
     */
    public function getCmsPageUrl(): string
    {
        return $this->page->getIdentifier();
    }
}
